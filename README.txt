
Module: DelLin (Delovye Linii)
Author: Jennifer Trelewicz <http://drupal.org/user/1601538>


Description
===========
Integrates the DelLin API with your website.

Requirements
============

* DelLin API key

Installation
============
Copy the 'dellin' module directory in to your Drupal sites/all/modules directory as usual.

If you will cache the catalogues lcally, then
launch cron (admin/config/system/cron) and follow the instructions in the system-generated email
for installing the large city and street tables. These are not loaded automatically, since they will timeout
your system, so must be loaded offline through mysql or pma.

!You will need to reenter the store address and shipping addresses of products to retrieve the KLADR!

Usage
=====
In the settings page enter your DelLin API key.
