<?php

/**
 * @file
 * Definition of variables for Variable API module.
 */

/**
 * Implements hook_variable_info().
 * 
 * @param $options
 * @return array of variables for dellin
 */
function dellin_variable_info($options) {
	$variables = array();
	$variables['dellin_api_key'] = array(
			'type' => 'string',
			'title' => t('API key', array(), $options),
			'description' => t('The API key in the form of xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx. To get an API key, <a href="@dellin">register with DelLin</a>.', array('@dellin' => 'http://dev.dellin.ru')),
			'required' => TRUE,
			'group' => 'dellin',
			'validate callback' => 'dellin_validate_dellin_api_key',
	);
	$variables['dellin_derival_door'] = array(
			'type' => 'boolean',
			'title' => t('Is door service needed from the shipper', array(), $options),
			'description' => t('Not set means that the packages are expected to be shipped from the nearest terminal.'),
			'group' => 'dellin',
			'default' => 1
	);
	$variables['dellin_url_countries'] = array(
			'type' => 'string',
			'title' => t('URL of the country catalogue', array(), $options),
			'required' => TRUE,
			'group' => 'dellin',
			'validate callback' => 'dellin_validate_dellin_url',
	);
	$variables['dellin_url_quote'] = array(
			'type' => 'string',
			'title' => t('URL of the quote calculator', array(), $options),
			'required' => TRUE,
			'group' => 'dellin',
			'validate callback' => 'dellin_validate_dellin_url',
	);
	$variables['dellin_url_places_search'] = array(
			'type' => 'string',
			'title' => t('URL for places search', array(), $options),
			'required' => TRUE,
			'group' => 'dellin',
			'validate callback' => 'dellin_validate_dellin_url',
	);
	$variables['dellin_url_streets_search'] = array(
			'type' => 'string',
			'title' => t('URL for streets search', array(), $options),
			'required' => TRUE,
			'group' => 'dellin',
			'validate callback' => 'dellin_validate_dellin_url',
	);
	
	return $variables;
}

/**
 * Implements hook_variable_group_info().
 * 
 * @return array of group information for the variables
 */
function dellin_variable_group_info() {
	$groups = array();
	$groups['dellin'] = array(
			'title' => t('DelLin'),
			'description' => t('Access to the DelLin API for Ubercart.'),
			'access' => 'administer dellin',
			'path' => array('admin/store/settings/quotes/settings/dellin'),
	);
	
	return $groups;
}

/**
 * Validate API key variable.
 *
 * @param array $variable
 * @return string
 */
function dellin_validate_dellin_api_key($variable) {
	// Replace all type of dashes (n-dash, m-dash, minus) with the normal dashes.
	$variable['value'] = str_replace(array('–', '—', '−'), '-', $variable['value']);
	
	if (!preg_match('/^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12}$/', $variable['value'])) {
		return t('A valid API key is case sensitive and formatted like xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxx.');
	}
}

/**
 * Callback to validate API URLs
 * 
 * @param array $variable to be validated
 */
function dellin_validate_dellin_url($variable) {
	if (!preg_match('/^https:\/\/api.dellin.ru/', $variable['value'])) {
		return t('Does not appear to be a valid API URL. Please indicate starting with https');
	}
}