<?php

define('_DELLIN_RF_UID', '0x8f51001438c4d49511dbd774581edb7a');

/**
 * Implements hook_menu()
 * 
 * @return array whose keys define paths and whose values are an associative array of properties for each path
 */
function dellin_menu() {
    $items = array();

	// Config page
	$items['admin/store/settings/quotes/settings/dellin'] = array(
			'title'            => 'DelLin',
			'description'      => 'DelLin base settings page',
			'page callback'    => 'drupal_get_form',
			'page arguments'   => array('dellin_admin_settings_form'),
			'access arguments' => array('configure quotes'),
			'type'             => MENU_LOCAL_TASK,
			'file'             => 'dellin.admin.inc',
	);
	
	return $items;
}

/**
 * Implements hook_form_FORM_ID_alter
 * @param $form: array of form elements that comprise the form.
 * @param $form_state: array containing the current state of the form.
 * The arguments that drupal_get_form() was originally called with are available
 * in the array $form_state['build_info']['args'].
 * @param $form_id: String representing the name of the form itself.
 * Typically this is the name of the function that generated the form.
 */
function dellin_form_uc_cart_checkout_form_alter(&$form, &$form_state, $form_id) {
	$form['panes']['quotes']['#description'] = t('Shipping quotes may be generated after 
indicating above the city and street for delivery. If you do not see quotes below or have updated 
the delivery address, press the button below. All quotes are estimates and are not final commercial 
offers until confirmed by our operator.');
}

/**
 * Implements hook_element_info_alter()
 * We override address options from Ubercart here for compatibility with the shipper
 * 
 * @param $type: array element type defaults as collected by hook_element_info().
 */
function dellin_element_info_alter(&$type) {
	$type['uc_address']['#process'][] = 'dellin_add_selects';
}

/**
 * Callback for dellin_element_info_alter()
 * @param $element: array element to alter
 * @param $form_state
 * @return array altered element
 */
function dellin_add_selects($element, &$form_state) {
	$countries = db_query('select * from {dellin_countries}');
	
	if (array_key_exists('delivery_city', $element)) {
		_dellin_add_selects_helper($element, $countries, 'delivery_');
	}
	if (array_key_exists('billing_city', $element)) {
		_dellin_add_selects_helper($element, $countries, 'billing_');
	}
	if (array_key_exists('city', $element)) {
		_dellin_add_selects_helper($element, $countries, '');
	}
	
	return $element;
}


/**
 * Helper function template for dellin_add_selects
 * @param $element
 * @param $countries: array of country strings
 * @param $pre: string prefix for the address elements in the form
 */
function _dellin_add_selects_helper(&$element, $countries, $pre) {
	$choose = t('- Choose -');
	$element[$pre.'country']['#options'] = array('0' => $choose);
	foreach ($countries as $row) {
		$element[$pre.'country']['#options'][$row->countryUID] = $row->country;
	}
	
	$element[$pre.'zone']['#required'] = FALSE;
	$element[$pre.'postal_code']['#required'] = FALSE;
	
	$element[$pre.'city']['#title'] = t('City or place'); 
	$element[$pre.'city']['#description'] = t('To facilitate quotes, please choose from the list. If your city is not in the list, then you may type it in, but you will need to contact our operator to get a shipping quote.'); 
	
	$element[$pre.'street1']['#title'] = t('Name of the street');
	$element[$pre.'street1']['#description'] = t('To facilitate quotes, please choose from the list, after choosing the city. If your street is not in the list, then you may type it in, but you will need to contact our operator to get a shipping quote.');
	$element[$pre.'street2']['#title'] = t('Number of building, apartment, office, etc.'); 
}

/**
 * Implements hook_kladr_form_city_search
 * 
 * @param string the code for selected $country
 * @param string $str to search for city
 * @return array empty if Russian Federation
 * otherwise array ('module key' => array of results; which empty if none, else city => city
 */
function dellin_kladr_form_city_search($country, $str) {
	$results = $matches = array();
	
	if ($country != _DELLIN_RF_UID) {
		$places_list = _dellin_search_city(trim($str));
		if (!empty($places_list)) {
			foreach (array_keys($places_list) as $name) {
				$matches[$name] = $name;
			}
		}
		$results['dellin'] = $matches;
		
		// We store this here for performance, and because on the API, we cannot search by complete place name
		$_SESSION['dellin_places_list'] = $places_list;
	}
	
	return $results;
}

/**
 * Helper function to get the matches for cities
 * @param string to search $str
 * @return array of matches keyed on the values
 */
function _dellin_search_city($str) {
	$matches = array();
	if (strlen($str) < 4)  {
		return $matches;
	}
	
	$key = variable_get('dellin_api_key');
	if (empty($key)) {
		return $matches;
	}
	
	$data = array(
			'appkey' => $key,
			'q' => $str,
			// no limit here because of pruning by code below
	);
	$options = array(
			'method' => 'POST',
			'data' => json_encode($data),
			'headers' => array('Content-Type' => 'application/json'),
	);
	$url = variable_get('dellin_url_places_search');
	$resp = drupal_http_request($url, $options);
	if (isset($resp->error)) {
		$vars = array(
				'@url' => $url,
				'@err' => $resp->error,
		);
		watchdog('dellin', 'Places search at @url gave error @err', $vars, WATCHDOG_WARNING);
	} elseif ($resp->code == 200) {
		$respdata = json_decode($resp->data, true);
		// By the documentation this array should be under key 'cities', but it is not.
		// Because this is an extension, we delete everything with code not starting with "999" 
		foreach($respdata as $row) {
			if (isset($row['code']) && substr($row['code'], 0, 3) == '999' && isset($row['aString'])) {
				$s = $row['aString'];
				if (!empty($row['cityID'])) $matches[$s]['cityID'] = $row['cityID'];
				$matches[$s]['code'] = $row['code'];
			}
		}
	}
	
	return $matches;
}

/**
 * Implements hook_kladr_form_street_search
 * @param string the code for selected $country
 * @param string of the $city
 * @param string $str to search for street
 * @return array empty if Russian Federation
 * otherwise array ('module key' => array of results; which empty if none, else city => city
 */
function dellin_kladr_form_street_search($country, $city, $str) {
	$results = $matches = array();
	
	if ($country != _DELLIN_RF_UID) {
		$places_list = $_SESSION['dellin_places_list'];
		if (!empty($places_list[$city]) and !empty($places_list[$city]['cityID'])) {
			$cityID = $places_list[$city]['cityID'];
		}
		
		$streets_list = _dellin_search_street($cityID, trim($str));
		if (!empty($streets_list)) {
			foreach (array_keys($streets_list) as $name) {
				$matches[$name] = $name;
			}
		}
		$results['dellin'] = $matches;
		
		// We store this here for performance, and because on the API, we cannot always search by complete street name
		$_SESSION['dellin_streets_list'] = $streets_list;
	}
	
	return $results;
}

/**
 * Helper function to get the matches for streets
 * @param string of cityID $cityID
 * @param string to search $str
 * @return array of matches keyed on the values
 */
function _dellin_search_street($cityID, $str) {
	$matches = array();
	
	if (empty($str) || strlen($str) < 3 || empty($cityID)) { // no city indicated; fail
		return $matches;
	}
	
	$key = variable_get('dellin_api_key');
	if (empty($key)) {
		return $matches;
	}
	
	$data = array(
			'appkey' => $key,
			'cityID' => $cityID,
			'street' => $str,
			'limit' => 50,
	);
	$options = array(
			'method' => 'POST',
			'data' => json_encode($data),
			'headers' => array('Content-Type' => 'application/json'),
	);
	$url = variable_get('dellin_url_streets_search');
	$resp = drupal_http_request($url, $options);
	if (isset($resp->error)) {
		$vars = array(
				'@url' => $url,
				'@err' => $resp->error,
		);
		watchdog('dellin', 'Streets search at @url gave error @err', $vars, WATCHDOG_WARNING);
	} elseif ($resp->code == 200) {
		$respdata = json_decode($resp->data, true);
		if (isset($respdata['streets'])) {
			foreach($respdata['streets'] as $row) {
				if(isset($row['aString'])) {
					$s = $row['aString'];
					if (isset($row['code'])) $matches[$s]['code'] = $row['code'];
				}
			}
		}
	}
	
	return $matches;
}

/**
 * Implements hook_uc_shipping_method()
 * 
 * @return array of shipping methods
 */
function dellin_uc_shipping_method() {
	$methods = array();
	$methods['dellin'] = array(
			'id' => 'dellin',
			'module' => 'dellin',
			'title' => t('DelLin'),
			'operations' => array(
					'configure' => array(
							'title' => t('configure'),
							'href' => 'admin/store/settings/quotes/settings/dellin',
					),
			),
			'quote' => array(
					'type' => 'small_package',
					'callback' => 'dellin_quote',
					'accessorials' => _dellin_service_types(),
			),
	);

	return $methods;
}

/**
 * Callback for retrieving a shipping quote.
 *
 * @param $products: Array of cart contents.
 * @param $details: array order details other than product information.
 * @param $method: array with the shipping method to create the quote.
 *
 * @return object JSON containing rate, error, and debugging information.
 */
function dellin_quote($products, $details, $method) {
	/*
	 * The uc_quote AJAX query can fire before the customer has completely
	 * filled out the destination address, so check to see whether the address
	 * has all needed fields. If not, abort.
	 */
	$destination = (object) $details;
	if (empty($destination->street1) || empty($destination->city)) {
		return array();
	}
	
	$quotes = array();
	foreach (array_keys(_dellin_service_types()) as $svckey) {
		$quotes[$svckey]['rate'] = 0;
	}

	$derival = variable_get('dellin_derival_door') + 0;
	
	$addresses = array(variable_get('uc_quote_store_default_address', new UcAddress()));
	if (count($addresses) < 1) {
		return array();
	}
	
	$to_kladr = _dellin_get_from_kladr($destination);
	if (empty($to_kladr)) return array();

	foreach ($products as $product) {
		$key = 0;
		$last_key = 0;
		$packages = array();
		
		if ($product->nid) {
			$address = uc_quote_get_default_shipping_address($product->nid);
			if (in_array($address, $addresses)) {
				// This is an existing address.
				foreach ($addresses as $index => $value) {
					if ($address == $value) {
						$key = $index;
						break;
					}
				}
			}
			else {
				// This is a new address.
				$addresses[++$last_key] = $address;
				$key = $last_key;
			}
		}
		if (!isset($product->pkg_qty) || !$product->pkg_qty) {
			$product->pkg_qty = 1;
		}
		$num_of_pkgs = (int) ($product->qty / $product->pkg_qty);
	
		/*
		 * Grab some product properties directly from the (cached) product
		 * data.  They are not normally available here because the $product
		 * object is being read out of the $order object rather than from
		 * the database, and the $order object only carries around a limited
		 * number of product properties.
		 */
		$temp = node_load($product->nid);
		$product->length = $temp->length;
		$product->width = $temp->width;
		$product->height = $temp->height;
		$product->length_units = $temp->length_units;
		$product->weight_units = $temp->weight_units;
		
		if ($num_of_pkgs) {
			$package = clone $product;
			$package->description = $product->model;
			$package->weight = $product->weight * $product->pkg_qty;
			$package->price = $product->price * $product->pkg_qty;
			$package->qty = $num_of_pkgs;
			if ($package->weight) {
				$packages[$key][] = $package;
			}
		}
		$remaining_qty = $product->qty % $product->pkg_qty;
		if ($remaining_qty) {
			$package = clone $product;
			$package->description = $product->model;
			$package->weight = $product->weight * $remaining_qty;
			$package->price = $product->price * $remaining_qty;
			$package->qty = 1;
			if ($package->weight) {
				$packages[$key][] = $package;
			}
		}
	}
	if (!count($packages)) {
		return array();
	}
	
	foreach ($packages as $key => $ship_packages) {
		$orig_kladr = _dellin_get_from_kladr($addresses[$key]);
		
		
		foreach ($ship_packages as $package) {
			// Construct the query and send
			$data = _dellin_setup_quote_query($orig_kladr, $to_kladr, $package, $derival);
			$url = variable_get('dellin_url_quote');
			if (empty($data) || empty($url)) {
				return array();
			}
			$options = array(
					'method' => 'POST',
					'data' => json_encode($data),
					'headers' => array('Content-Type' => 'application/json'),
			);
			$resp = drupal_http_request($url, $options);
			if (isset($resp->error)) {
				$vars = array(
						'@url' => $url,
						'@err' => $resp->error,
				);
				watchdog('dellin', 'Quote at @url failed with error @err', $vars, WATCHDOG_WARNING);
				return array();
			} elseif ($resp->code == 200) {
				$respdata = json_decode($resp->data, true);
				_dellin_parse_quote_response($respdata, $quotes);
			}
		}
	}
	// Sort quotes by price, lowest to highest.
	uasort($quotes, 'uc_quote_price_sort');
	
	foreach ($quotes as $key => $quote) {
		if (isset($quote['rate'])) {
			$quotes[$key]['rate'] = $quote['rate'];
			$quotes[$key]['label'] = $method['quote']['accessorials'][$key];
			$quotes[$key]['option_label'] = t('DelLin: ') . $method['quote']['accessorials'][$key];
		}
	}
	
	return $quotes;
}

/**
 * Helper function to return potential search strings for dellin API
 * @param string complete city/stree name $fullstr
 * @return array of potential search strings
 */
function _dellin_retrieve_search_strings($fullstr) {
	$queries = array();
	
	// strip off everything after (, since these are region names
	$a = explode('(', $fullstr);
	$fullstr = $a[0];
	
	// break into words and include only those with capital letters
	$a = explode(' ', $fullstr);
	foreach ($a as $s) {
		if (mb_strlen($s) < 3) continue;
		// test for uppercase language-independent
		$c = mb_substr($s, 0, 1);
		if ($c == mb_strtoupper($c)) $queries[] = $s;
	}
	
	return $queries;
}

/**
 * Helper function to get KLADR from addresses
 * @param object UcAddress $orig
 * @return string with KLADR if found
 */
function _dellin_get_from_kladr($orig) {
	$orig_kladr = '';
	if ($orig->country == _DELLIN_RF_UID) {
		$orig_kladr = kladr_form_code_from_name($orig->city, $orig->street1);
	} else {
		$key = variable_get('dellin_api_key');
		if (!empty($key)) {
			/*
			 * The API only lets us search on "optimized" strings, and Ubercart does
			 * not store KLADR, so here we have this messy approach.
			 */
			$cityID = NULL;
			$queries = _dellin_retrieve_search_strings($orig->city);
			foreach ($queries as $q) {
				$list = _dellin_search_city($q);
				foreach ($list as $name => $row) {
					if ($name == $orig->city and !empty($row['cityID'])) {
						$cityID = $row['cityID'];
						break;
					}
				}
				if (!empty($cityID)) break;
			}
			
			if (!empty($cityID)) {	// now the street
				$queries = _dellin_retrieve_search_strings($orig->street1);
				foreach ($queries as $q) {
					$list = _dellin_search_street($cityID, $q);
					foreach ($list as $name => $row) {
						if ($name == $orig->street1 and !empty($row['code'])) {
							$orig_kladr = $row['code'];
							break;
						}
					}
					if (!empty($orig_kladr)) break;
				}
			}
		}
	}
		
	return $orig_kladr;
}

/**
 * Helper function for setting up the query by template
 * @param the КЛАДР code for the originator: $orig_kladr
 * @param the КЛАДР code for the destination: $to_kladr
 * @param the packages to be sent: $package
 * @param TRUE if derival services are needed: $derival
 * @return object query
 */
function _dellin_setup_quote_query($orig_kladr, $to_kladr, $package, $derival = FALSE) {
	$apikey = variable_get('dellin_api_key');
	if (empty($apikey)) {
		return;
	}
	
	// Need to fill out the KLADR to 25 positions
	$orig_kladr = str_pad($orig_kladr, 25, '0', STR_PAD_RIGHT);
	$to_kladr = str_pad($to_kladr, 25, '0', STR_PAD_RIGHT);
	
	switch ($package->weight_units) {
		case 'kg':
			$wt_scale = 1.0;
			break;
		case 'g':
			$wt_scale = 0.01;
			break;
		case 'lb':
			$wt_scale = 0.453592;
			break;
		case 'oz':
			$wt_scale = 0.0283495;
			break;
		default:
			return;
	}
	switch ($package->length_units) {
		case 'cm':
			$len_scale = 0.01;
			break;
		case 'mm':
			$len_scale = 0.001;
			break;
		case 'ft':
			$len_scale = 0.3048;
			break;
		case 'in':
			$len_scale = 0.0254;
			break;
		default:
			return;
	}
	$vol_scale = $len_scale * $len_scale * $len_scale;
	$data = array(
			'appkey' => $apikey,
			'derivalPoint' => $orig_kladr,
			'derivalDoor' => (bool) $derival,
			'arrivalPoint' => $to_kladr,
			'arrivalDoor' => TRUE,
			'sizedVolume' => $package->length * $package->width * $package->height * $package->qty * $vol_scale,
			'sizedWeight' => $package->weight * $wt_scale,
			'statedValue' => $package->price,
			'packages_count' => $package->qty,
	);
	
	return $data;
}

/**
 * Helper function for parsing the quote response from the dellin API
 * @param response from the API: $data
 * @param information for the Ubercart interface: $quotes
 */
function _dellin_parse_quote_response($data, &$quotes) {
	if (isset($data['errors']) and isset($data['errors']['messages'])) {
		$vars = array(
				'@err' => implode('; ', $data['errors']['messages']),
		);
		watchdog('dellin', 'Quote failed with error @err', $vars, WATCHDOG_WARNING);
	}

	foreach (array_keys(_dellin_service_types()) as $svckey) {
		if (!isset($data[$svckey])) {
			/*
			 * All or nothing quote. If some products can't be shipped by
			 * a certain service, no quote is given for that service. If
			 * that means no quotes are given at all, they'd better call in.
			 */
			unset($quotes[$svckey]);
		} else if (isset($quotes[$svckey])) {
			$quotes[$svckey]['rate'] += $data[$svckey]['price'] + $data['arrival']['price'] + $data['derival']['price'];
			if (isset($data[$svckey]['insurance'])) $quotes[$svckey]['rate'] += $data[$svckey]['insurance'];
			if (isset($data[$svckey]['notify'])) $quotes[$svckey]['rate'] += $data[$svckey]['notify']['price'];
		}
	}
}

/**
 * Helper function to provide array of strings of valid service types. Key is machine key, value is description.
 * @return array of service types
 */
function _dellin_service_types() {
	return array(
			'intercity' => t('Terminal-to-terminal shipping'), 
			'small' => t('Delivery of small packages'), 
			'air' => t('Air shipping'), 
			'express' => t('Express delivery'), 
			'letter' => t('Letter delivery service'), 
	);
}

/**
 * Implements hook_cron
 */
function dellin_cron() {
	$key = variable_get('dellin_api_key');
	if (empty($key)) return;
	
	$result = db_query('SELECT * FROM {dellin_catalogues}');
	$data = array('appkey' => $key);
	$options = array(
			'method' => 'POST',
			'data' => json_encode($data),
			'headers' => array('Content-Type' => 'application/json'),
	);
	foreach ($result as $row) {
		$url = variable_get($row->url_var);
		$resp = drupal_http_request($url, $options);
		if (isset($resp->error)) {
			$vars = array(
					'@url' => $url,
					'@err' => $resp->error,
					'@tab' => $row->tab
			);
			watchdog('dellin', 'Verify @tab at @url gave error @err', $vars, WATCHDOG_WARNING);
		} elseif ($resp->code == 200) {
			$respdata = json_decode($resp->data, true);
			if (strcmp($row->hash, $respdata['hash'])) {
				_dellin_update_catalogue($row, $respdata);
			}
		}
	}
}

/**
 * Helper function to update cached catalogue in the database
 * @param $row object from the database
 * @param $data array from the API
 */
function _dellin_update_catalogue($row, $data) {
	$o = array(
			'method' => 'GET',
			'headers' => array('Content-Type' => 'application/octet-stream'),
	);
	$file = drupal_http_request(drupal_strip_dangerous_protocols($data['url']), $o);
	if ($file->code == 200 && isset($file->data)) {
		$lines = explode(PHP_EOL, $file->data);

		// Update the database
		$txn = db_transaction();
		$keys = NULL;
		$skipped = 0;
		try {
			db_truncate($row->tab)->execute();
			foreach ($lines as $line) {
				$temp = str_getcsv($line);
				if (empty($keys)) {
					if (count($temp) > 0) $keys = $temp;
				} else {
					if (count($temp) == count($keys)) {
						db_insert($row->tab)->fields(array_combine($keys, $temp))->execute();
					} else {
						if (count($temp) > 0) ++$skipped;	// ignore null lines
					}
				}
			}
				
			db_update('dellin_catalogues')
			->fields(array(
					'hash' => $data['hash'],
					'updated' => REQUEST_TIME,
			))
			->condition('tab', $row->tab, '=')->execute();
			$vars = array('@tab' => $row->tab, '@skipped' => $skipped);
			watchdog('dellin', 'Updated @tab with @skipped skipped lines', $vars);
		}
		catch (Exception $e) {
			$txn->rollback();
			watchdog_exception('dellin', $e);
		}
	} else {
		$vars = array(
				'@url' => $data['url'],
				'@clean_url' => drupal_strip_dangerous_protocols($data['url']),
				'@err' => $file->code,
				'@tab' => $row->tab
		);
		watchdog('dellin', 'Update @tab at @url (@clean_url) gave error @err', $vars, WATCHDOG_WARNING);
	}
}
