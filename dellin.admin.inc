<?php

/**
 * @file
 * Administrative page callbacks for the dellin module.
 */

/**
 * Implements hook_admin_settings() for module settings configuration.
 * @param $form_state
 */
function dellin_admin_settings_form($form_state) {
	$form = array();
	$form['dellin_api_key'] = array(
			'#title' => t('API key'),
			'#type' => 'textfield',
			'#default_value' => variable_get('dellin_api_key'),
			'#size' => 36,
			'#maxlength' => 36,
			'#required' => TRUE,
			'#description' => t('The API key in the form of xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx. To get an API key, <a href="@dellin">register with DelLin</a>.', array('@dellin' => 'http://dev.dellin.ru')),
	);
	$form['dellin_derival_door'] = array(
			'#type' => 'checkbox',
			'#title' => t('Is door service needed from the shipper'),
			'#description' => t('Not set means that the packages are expected to be shipped from the nearest terminal.'),
			'#default_value' => variable_get('dellin_derival_door'),
	);
	
	$form['urls'] = array(
			'#type' => 'fieldset',
			'#title' => t('URLs for the API'),
			'#collapsible' => TRUE,
			'#collapsed' => TRUE
	);
	
	$form['urls']['dellin_url_countries'] = array(
			'#type' => 'textfield',
			'#size' => 64,
			'#title' => t('URL of the country catalogue'),
			'#required' => TRUE,
			'#default_value' => variable_get('dellin_url_countries'),
	);
	$form['urls']['dellin_url_quote'] = array(
			'#type' => 'textfield',
			'#size' => 64,
			'#title' => t('URL of the quote calculator'),
			'#required' => TRUE,
			'#default_value' => variable_get('dellin_url_quote'),
	);
	$form['urls']['dellin_url_places_search'] = array(
			'#type' => 'textfield',
			'#size' => 64,
			'#title' => t('URL for places search'),
			'#required' => TRUE,
			'#default_value' => variable_get('dellin_url_places_search'),
	);
	$form['urls']['dellin_url_streets_search'] = array(
			'#type' => 'textfield',
			'#size' => 64,
			'#title' => t('URL for streets search'),
			'#required' => TRUE,
			'#default_value' => variable_get('dellin_url_streets_search'),
	);
	
	return system_settings_form($form);
}

/**
 * Implements hook_form_validate().
 * @param $form
 * @param $form_state
 */
function dellin_admin_settings_form_validate($form, &$form_state) {
	$form_state['values']['dellin_api_key'] = trim($form_state['values']['dellin_api_key']);
	// Replace all type of dashes (n-dash, m-dash, minus) with the normal dashes.
	$form_state['values']['dellin_api_key'] = str_replace(array('–', '—', '−'), '-', $form_state['values']['dellin_api_key']);
	if (!preg_match('/^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12}$/', $form_state['values']['dellin_api_key'])) {
		form_set_error('dellin_api_key', t('A valid API key is case sensitive and formatted like xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx.'));
	}
	
	$a = array('dellin_url_countries', 'dellin_url_places_search', 'dellin_url_streets_search', 'dellin_url_quote');
	foreach ($a as $var) {
		$form_state['values'][$var] = trim($form_state['values'][$var]);
		if (!preg_match('/^https:\/\/api.dellin.ru/', $form_state['values'][$var])) {
			form_set_error($var, t('Does not appear to be a valid API URL. Please indicate starting with https'));
		}
	}
}
